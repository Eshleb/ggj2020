﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class MainCamera : MonoBehaviour
    {
        #region Singleton
        public static MainCamera Instance { get; private set; }
        public static Camera InstanceCam { get; private set; }
        void Awake()
        {
            if (null != Instance)
                throw new System.Exception("More than one mainCamera.");
            Instance = this;
            InstanceCam = GetComponent<Camera>();
            Material = new Material(Shader.Find("Hidden/HueShader"));
        }
        #endregion

        private Material Material;
        [Range(0, 1)]
        public float Factor;

        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
            Material.SetFloat("_Factor", Factor);
            Graphics.Blit(source, destination, Material);
        }

        public void FlashColor(Color color, float flashTime, float maxFac)
        {
            Material.SetColor("_Color", color);
            StartCoroutine(FlashColorCoroutine(flashTime, maxFac));
        }

        private IEnumerator FlashColorCoroutine(float flashTime, float maxFac)
        {
            float realFlashtime = flashTime / 2;

            float timer = 0;

            while (timer <= realFlashtime)
            {

                Factor = maxFac * timer / realFlashtime;
                Material.SetFloat("_Factor", Factor);
                yield return null;
                timer += Time.deltaTime;
            }

            while (0 <= timer)
            {
                Factor = maxFac * timer / realFlashtime;
                Material.SetFloat("_Factor", Factor);
                yield return null;
                timer -= Time.deltaTime;
            }
        }
    }
}