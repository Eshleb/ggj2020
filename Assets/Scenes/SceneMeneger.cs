﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneMeneger : MonoBehaviour
{
 
    public void NextScene()
    {
        SceneManager.LoadScene("Game");
    }
    public void LoadCredit()
    {
        SceneManager.LoadScene("Credits");
    }
    public void LoadMain()
    {
        SceneManager.LoadScene("Main");
    }
}

