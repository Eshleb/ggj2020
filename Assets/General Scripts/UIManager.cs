﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class UIManager : MonoBehaviour
    {
        public static UIManager Instance { get; private set; }

        public event System.Action ReadyForNextTurn;

        public Sprite FirePlayerSprite, WaterPlayerSprite, EarthPlayerSprite;
        public SpriteRenderer TurnIndicatorHue;

        public Color[] hueColors;
        public float TimeBetweenTurns;

        void Awake()
        {
            if (Instance == null)
                Instance = this;
            else
                Destroy(gameObject);

            GameLoopManager.OnNewTurn += UpdateCurrentPlayer;
            GameLoopManager.OnRoundResolved += UpdateCurrentPlayer;
        }

        public Button ConfirmButton;

        public void SetConfirmButtonInteractable(bool Interactable)
        {
            ConfirmButton.gameObject.SetActive(true);
            ConfirmButton.interactable = Interactable;
        }

        public void SetConfirmButtonActive(bool Active)
        {
            ConfirmButton.gameObject.SetActive(Active);
        }

        public void UpdateCurrentPlayer()
        {
            if (null == Player.CurrentlyPlaying)
                StartCoroutine(ChangeTurnColors(Color.white));
            else 
                StartCoroutine(ChangeTurnColors(hueColors[(int)Player.CurrentlyPlaying.Element]));
        }

        private IEnumerator ChangeTurnColors(Color TargetColor)
        {
            Color col1 = TurnIndicatorHue.color,
                  col2 = TargetColor;

            float timer = 0;

            while (timer <= TimeBetweenTurns)
            {
                TurnIndicatorHue.color = Color.Lerp(col1, col2, timer / TimeBetweenTurns);
                timer += Time.deltaTime;

                yield return null;
            }

            ReadyForNextTurn?.Invoke();
        }

    }
}