﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game;
using World;
using UnityEngine.SceneManagement;

namespace Game
{
    public static class GameLoopManager
    {
        public static List<Player> playerList;

        public static event System.Action OnRoundResolved, OnNewRound;
        public static event System.Action OnTurnResolved, OnNewTurn;

        public static int CurrentRound { get; private set; }

        private static bool wasBalancedLastRound = false;

        static GameLoopManager()
        {
            playerList = new List<Player>();
        }

        public static void AddPlayer(Player Player)
        {
            playerList.Add(Player);
            Player.OnConfirmAction += OnPlayerConfirmedAction;
            if (playerList.Count == 3)
                Announcer.Instance.Announce(1.5f, 1f, "Begin!", playerList[0].Element.ToString() + " player goes first!", StartRound);
        }

        private static void StartRound()
        {
            CurrentRound++;
            if (2 < CurrentRound)
            {
                if (WorldMap.Instance.RoundsUntilImpact == 0)
                {
                    WorldMap.Instance.AnnounceRandomEvent();
                }
            }
            //else
            //change number of rounds until impact in scene
            OnNewRound?.Invoke();
            Player.CurrentlyPlaying = playerList[0];
            OnNewTurn?.Invoke();
            if (null != Audiomaneger.Instance)
                Audiomaneger.Instance.Changemusic(CurrentRound);
        }


        public static void ResolveRound()
        {
            GameManager.Instance.DelayAction(0.3f, CardApplicator.Instance.ApplyCard); //calls ApplyNextCard when finished applying
        }

        public static void ApplyNextCard()
        {
            WorldMap.Instance.ChangeElementValueNumber();
            if (CardApplicator.Instance.Done)
            {
                if (2 < CurrentRound)
                {
                    WorldMap.Instance.RoundsUntilImpact--;
                    if (WorldMap.Instance.RoundsUntilImpact == 0)
                    {
                        WorldMap.Instance.PlayRandomEvent();
                    }
                }

                CheckForWinCondition();
                OnRoundResolved?.Invoke();


                playerList.Add(playerList[0]);
                playerList.RemoveAt(0);
                Announcer.Instance.Announce(1.5f, 1f, "Round " + (CurrentRound + 1) + "!", playerList[0].Element.ToString() + " player goes first!", StartRound);

                if (wasBalancedLastRound)
                    Announcer.Instance.Announce(1.5f, 1f, "You have reached harmony!", "Stay balanced until the end of this round!");
                
            }
            else GameManager.Instance.DelayAction(0.7f, CardApplicator.Instance.ApplyCard); //calls ApplyNextCard when finished applying
        }

        public static void OnPlayerConfirmedAction()
        {
            if (playerList.TrueForAll(pl => pl.IsReady))
            {
                Player.CurrentlyPlaying = null;
                UIManager.Instance.SetConfirmButtonActive(false);
                ResolveRound();
            }

            else
            {
                Player.CurrentlyPlaying = playerList[playerList.IndexOf(Player.CurrentlyPlaying) + 1];
                OnNewTurn?.Invoke();
            }
        }


        public static void CheckForWinCondition()
        {
            int[] elementalSum = WorldMap.Instance.GetElementalBalance();
            if (30 < elementalSum[0] && elementalSum[0] < 38 && 30 < elementalSum[1] && elementalSum[1] < 38 && 30 < elementalSum[2] && elementalSum[2] < 38)
            {
                if (wasBalancedLastRound)
                {
                    Victory(0);
                }
                else wasBalancedLastRound = true;
            }
            else wasBalancedLastRound = false;

            if (elementalSum[0] > 60)
            {
                Victory(1);
            }
            if (elementalSum[1] > 60)
            {
                Victory(2);
            }
            if (elementalSum[2] > 60)
            {
                Victory(3);
            }
        }

        public static void Victory(int whoWon)
        {
            switch (whoWon)
            {
                case 0:
                {
                    SceneManager.LoadScene("All");
                    break;
                }
                case 1:
                {
                    SceneManager.LoadScene("Fire");
                    break;
                }

                case 2:
                {
                    SceneManager.LoadScene("Water");
                    break;
                }
                case 3:
                {
                    SceneManager.LoadScene("Earth");
                    break;
                }
            }
        }
    }
}
