﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cards
{
    [System.Serializable]
    public class Hitzone
    {
        public Vector2Int Size, Center;
        public BoolArray[] Zone;
        [System.Serializable]
        public class BoolArray
        {
            public bool[] Array;
        }
        [HideInInspector, SerializeField] private bool open;

        public bool this[int x, int y] { get { return Zone[y].Array[x]; } }

        public Hitzone(Hitzone Other)
        {
            Size = Other.Size;
            Center = Other.Center;
            Zone = new BoolArray[Other.Zone.Length];
            for (int i = 0; i < Zone.Length; i++)
            {
                Zone[i] = new BoolArray() { Array = new bool[Other.Zone[i].Array.Length] };
                Other.Zone[i].Array.CopyTo(Zone[i].Array, 0);
            }
        }
    }
}