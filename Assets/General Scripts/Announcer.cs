﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class Announcer : MonoBehaviour
    {
        public GameObject Popup;
        public UnityEngine.UI.Image PopupSprite;
        public TMPro.TextMeshProUGUI Title, Description;

        private Coroutine announcementCoruotine;

        private struct AnnouncementData
        {
            public float StaticTime, FadeTime;
            public string TitleText, DescriptionText;
            public System.Action CallbackAfterFade;

            public AnnouncementData(float StaticTime, float FadeTime, string TitleText, string DescriptionText = "", System.Action CallbackAfterFade = null)
            {
                this.StaticTime = StaticTime;
                this.FadeTime = FadeTime;
                this.TitleText = TitleText;
                this.DescriptionText = DescriptionText;
                this.CallbackAfterFade = CallbackAfterFade;
            }
        }

        private Queue<AnnouncementData> announcementQueue = new Queue<AnnouncementData>();

        public static Announcer Instance { get; private set; }
        private void Awake()
        {
            Instance = this;
        }

        public void Announce(float StaticTime, float FadeTime, string TitleText, string DescriptionText = "", System.Action CallbackAfterFade = null)
        {
            if (null == announcementCoruotine && 0 == announcementQueue.Count)
            {

                Popup.SetActive(true);
                Title.text = TitleText;
                Description.text = DescriptionText;
                announcementCoruotine = StartCoroutine(FadeCRTN(StaticTime, FadeTime, CallbackAfterFade));
            }
            else announcementQueue.Enqueue(new AnnouncementData(StaticTime, FadeTime, TitleText, DescriptionText, CallbackAfterFade));
        }

        private void Announce(AnnouncementData Data)
        {
            Announce(Data.StaticTime, Data.FadeTime, Data.TitleText, Data.DescriptionText, Data.CallbackAfterFade);
        }

        private IEnumerator FadeCRTN(float StaticTime, float FadeTime, System.Action Callback)
        {
            float appearStartTime = Time.time;
            float appearTime = FadeTime * 0.5f;
            while (Time.time - appearStartTime < appearTime)
            {
                SetAlpha((Time.time - appearStartTime) / appearTime);
                yield return null;
            }
            SetAlpha(1f);
            yield return new WaitForSeconds(StaticTime);
            float fadeStartTime = Time.time;
            while (Time.time - fadeStartTime < FadeTime)
            {
                SetAlpha(1 - (Time.time - fadeStartTime) / FadeTime);
                yield return null;
            }
            SetAlpha(0);
            Popup.SetActive(false);
            Callback?.Invoke();
            if (0 < announcementQueue.Count)
            {
                GameManager.Instance.DelayAction(0.5f, Announce, announcementQueue.Dequeue());
            }
            announcementCoruotine = null;
        }

        private void SetAlpha(float alpha)
        {
            PopupSprite.color = Title.color = Description.color =
                new Color(1, 1, 1, alpha);
        }
    }
}