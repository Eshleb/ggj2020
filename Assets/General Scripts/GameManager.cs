﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance { get; private set; }

        void Awake()
        {
            if (Instance == null)
                Instance = this;
            else
                Destroy(gameObject);
        }

        public void ConfirmAction()
        {
            Player.CurrentlyPlaying.ConfirmAction();
        }

        public void DelayAction(float time, System.Action Action)
        {
            StartCoroutine(DelayActionCRTN(time, Action));
        }
        private IEnumerator DelayActionCRTN(float time, System.Action action)
        {
            yield return new WaitForSeconds(time);
            action?.Invoke();
        }
        public void DelayAction<T>(float time, System.Action<T> Action, T Param)
        {
            StartCoroutine(DelayActionCRTN(time, Action, Param));
        }
        private IEnumerator DelayActionCRTN<T>(float time, System.Action<T> action, T Param)
        {
            yield return new WaitForSeconds(time);
            action?.Invoke(Param);
        }
        public void DelayAction<T1, T2>(float time, System.Action<T1, T2> Action, T1 Param1, T2 Param2)
        {
            StartCoroutine(DelayActionCRTN(time, Action, Param1, Param2));
        }
        private IEnumerator DelayActionCRTN<T1, T2>(float time, System.Action<T1, T2> action, T1 Param1, T2 Param2)
        {
            yield return new WaitForSeconds(time);
            action?.Invoke(Param1, Param2);
        }
    }
}