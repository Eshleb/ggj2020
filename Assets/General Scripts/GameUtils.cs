﻿using Cards;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using World;

namespace Utils
{
    public static class GameUtils
    {

        public static List<WorldTile> GetAffectedTiles(WorldTile[,] tiles, Hitzone hitZone, Vector2Int startTile)
        {
            var inHitzone = new List<WorldTile>();

            Vector2Int startIndex = startTile - hitZone.Center;



            for (int i = 0; i < hitZone.Size.y; i++)
            {
                if (startIndex.y + i >= 0 && startIndex.y + i < tiles.GetLength(0))
                    for (int j = 0; j < hitZone.Size.x; j++)
                        if (startIndex.x + j >= 0 && startIndex.x + j < tiles.GetLength(1))
                            if (hitZone[j, i])
                                inHitzone.Add(tiles[startIndex.x + j, startIndex.y + i]);
            }

            return inHitzone;
        }


        public static int[] CalculateCardEffect(Cards.Card Card, int[] CurrentElementProminence)
        {
            //fire beats earth, earth beats water, water beats fire
            int[] newProminence = CopyArray(CurrentElementProminence[(int)Element.Fire], CurrentElementProminence[(int)Element.Water], CurrentElementProminence[(int)Element.Earth]);
            int[] cardEffects = CopyArray(Card.FireEffect, Card.WaterEffect, Card.EarthEffect);
            Element mainElement = (Element)(-1), secondaryElement = (Element)(-1);
            if (cardEffects[(int)Element.Fire] > 0)
                mainElement = Element.Fire;

            if (cardEffects[(int)Element.Water] > 0)
            {
                if (Element.Fire == mainElement)
                {
                    secondaryElement = Element.Fire;
                }
                mainElement = Element.Water;
            }
            if (cardEffects[(int)Element.Earth] > 0)
            {
                if (Element.Fire == mainElement)
                    secondaryElement = Element.Earth;
                else if (Element.Water == mainElement)
                {
                    secondaryElement = Element.Water;
                    mainElement = Element.Earth;
                }
                else mainElement = Element.Earth;
            }

            while (0 < cardEffects[(int)mainElement])
            {
                ApplyEffect(mainElement, newProminence);
                cardEffects[(int)mainElement]--;
            }
            if ((Element)(-1) != secondaryElement)
            {
                while (0 < cardEffects[(int)secondaryElement])
                {
                    ApplyEffect(secondaryElement, newProminence);
                    cardEffects[(int)secondaryElement]--;
                }
            }
            return newProminence;

            #region old model
            /*newProminence[(int)Element.Fire] = Mathf.Clamp(newProminence[(int)Element.Fire] - cardEffects[(int)Element.Water], 0, 3);
            newProminence[(int)Element.Water] = Mathf.Clamp(newProminence[(int)Element.Water] - cardEffects[(int)Element.Earth], 0, 3);
            newProminence[(int)Element.Earth] = Mathf.Clamp(newProminence[(int)Element.Earth] - cardEffects[(int)Element.Fire], 0, 3);
            cardEffects[(int)Element.Fire] = Mathf.Clamp(cardEffects[(int)Element.Fire] - newProminence[(int)Element.Water], 0, 3);
            cardEffects[(int)Element.Water] = Mathf.Clamp(cardEffects[(int)Element.Water] - newProminence[(int)Element.Earth], 0, 3);
            cardEffects[(int)Element.Earth] = Mathf.Clamp(cardEffects[(int)Element.Earth] - newProminence[(int)Element.Fire], 0, 3);

            int[] res =
                    CopyArray(
                Mathf.Clamp(newProminence[(int)Element.Fire] + cardEffects[(int)Element.Fire], 0, 3),
                Mathf.Clamp(newProminence[(int)Element.Water] + cardEffects[(int)Element.Water], 0, 3),
                Mathf.Clamp(newProminence[(int)Element.Earth] + cardEffects[(int)Element.Earth], 0, 3)
            );
            if (res[0] + res[1] + res[2] > 3)
            {
                Element zero = (Element)(-1), three = (Element)(-1);
                for (int i = 0; i < 3; i++)
                    if (res[i] == 0)
                        zero = (Element)i;
                    else if (res[i] == 3)
                        three = (Element)i;
                switch (three)
                {
                    case (Element)(-1):
                    {
                        switch (zero)
                        {
                            case Element.Fire: //fire is zero, competing are water and earth, earth wins
                            res[(int)Element.Water] = 1;
                            break;
                            case Element.Water: //water is zero, competing are fire and earth, fire wins
                            res[(int)Element.Earth] = 1;
                            break;
                            case Element.Earth: //earth is zero, competing are water and fire, water wins
                            res[(int)Element.Fire] = 1;
                            break;
                        }
                        break;
                    } 
                    //if one element is a three, reset both others
                    case Element.Fire:
                    {
                        res[(int)Element.Water] = 0;
                        res[(int)Element.Earth] = 0;
                        break;
                    }
                    case Element.Earth:
                    {
                        res[(int)Element.Water] = 0;
                        res[(int)Element.Fire] = 0;
                        break;
                    }
                    case Element.Water:
                    {
                        res[(int)Element.Fire] = 0;
                        res[(int)Element.Earth] = 0;
                        break;
                    }
                }
            }
            return res;*/
            #endregion old model
        }

        public static void ApplyEffect(Element Element, int[] prominence)
        {
            int sum = prominence[0] + prominence[1] + prominence[2];
            if (sum < 3)
                prominence[(int)Element] += 1;
            else
            {
                int victimIndex = (int)GetStrongAgainst(Element);
                if (0 < prominence[victimIndex])
                {
                    prominence[victimIndex] -= 1;
                }
            }
        }

        private static int[] CopyArray(int Fire, int Water, int Earth)
        {
            return new int[] { Fire, Water, Earth };
        }

        public static Element GetStrongAgainst(Element element)
        {
            switch (element)
            {
                case Element.Fire:
                return Element.Earth;
                case Element.Water:
                return Element.Fire;
                case Element.Earth:
                return Element.Water;
            }
            return (Element)(-1);
        }

        public static Element GetWeakAgainst(Element element)
        {
            switch (element)
            {
                case Element.Fire:
                return Element.Water;
                case Element.Water:
                return Element.Earth;
                case Element.Earth:
                return Element.Fire;
            }
            return (Element)(-1);
        }
    }
}