﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Cards.Editor
{
    [CustomPropertyDrawer(typeof(Hitzone))]
	public class HitzoneEditor : PropertyDrawer
	{
        private SerializedProperty open;
        private SerializedProperty sizeP, centerP, zoneP;
        private GUIStyle centerStyle;

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            if (null == sizeP)
            {
                sizeP = property.FindPropertyRelative("Size");
                centerP = property.FindPropertyRelative("Center");
                zoneP = property.FindPropertyRelative("Zone");
                open = property.FindPropertyRelative("open");
                centerStyle = EditorStyles.toggle;
                centerStyle = new GUIStyle(centerStyle);
                centerStyle.normal.background = Texture2D.redTexture;
                centerStyle.hover.background = Texture2D.redTexture;
                centerStyle.onNormal.background = Texture2D.redTexture;
                centerStyle.onHover.background = Texture2D.redTexture;
                centerStyle.onActive.background = Texture2D.redTexture;
                centerStyle.focused.background = Texture2D.redTexture;
                centerStyle.onFocused.background = Texture2D.redTexture;
                centerStyle.active.background = Texture2D.redTexture;
            }
            
            if (!open.boolValue)
                return 16;

            return 51 + 19 * sizeP.vector2IntValue.y;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            Rect p = new Rect(position);
            p.height = 16;
            bool wasOpen = open.boolValue;
            open.boolValue = EditorGUI.Foldout(p, open.boolValue, "Hitzone", true);
            if (!wasOpen)
                return;
            

            p.y += 19;
            p.xMin += 15;
            EditorGUI.PropertyField(p, sizeP);
            p.y += 19;
            EditorGUI.PropertyField(p, centerP);
            p.y += 19;
            Vector2Int size = sizeP.vector2IntValue, center = centerP.vector2IntValue;
            size = new Vector2Int(Mathf.Clamp(size.x, 0, 8), Mathf.Clamp(size.y, 0, 8));
            sizeP.vector2IntValue = size;
            center = new Vector2Int(Mathf.Clamp(center.x, 0, Mathf.Max(size.x-1, 0)), Mathf.Clamp(center.y, 0, Mathf.Max(size.y-1, 0)));
            centerP.vector2IntValue = center;
            zoneP.arraySize = size.y;
            for (int i = 0; i < size.y; i++)
            {
                SerializedProperty lineP = zoneP.GetArrayElementAtIndex(i).FindPropertyRelative("Array");
                lineP.arraySize = size.x;
                Rect chbxPos = new Rect(p);
                chbxPos.y += 19 * i;
                chbxPos.xMin += 15;
                chbxPos.width = 16;
                for (int j = 0; j < size.x; j++)
                {
                    SerializedProperty box = lineP.GetArrayElementAtIndex(j);
                    chbxPos.x += 16;
                    if (center.x == j && center.y == i)
                    {
                        chbxPos.xMin -= 1;
                        chbxPos.xMax += 5;
                        chbxPos.yMin -= 1;
                        chbxPos.yMax += 1;
                    }
                    box.boolValue = EditorGUI.Toggle(chbxPos, box.boolValue, center.x == j && center.y == i ? centerStyle : EditorStyles.toggle);
                    if (center.x == j && center.y == i)
                    {
                        chbxPos.xMin += 1;
                        chbxPos.xMax -= 1;
                        chbxPos.yMin += 1;
                        chbxPos.yMax -= 1;
                    }
                }
            }
        }
    }
}