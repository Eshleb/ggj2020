﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Utils
{
    public static class Tests
    {
        [MenuItem("Test/Test elements")]
        public static void TestCalculation()
        {
            System.Text.StringBuilder res = new System.Text.StringBuilder();
            int[] elements = new int[3];
            Cards.Card card = ScriptableObject.CreateInstance<Cards.Card>();
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    for (int k = 0; k < 4; k++)
                    {
                        for (int l = 0; l < 4; l++)
                        {
                            for (int m = 0; m < 4; m++)
                            {
                                for (int n = 0; n < 4; n++)
                                {
                                    elements[0] = i;
                                    elements[1] = j;
                                    elements[2] = k;
                                    card.FireEffect = l;
                                    card.WaterEffect = m;
                                    card.EarthEffect = n;
                                    int a = Sum(elements), b = Sum(card);
                                    if (a == 0 || a > 3 || b == 0 || b > 3)
                                        continue;
                                    int[] result = GameUtils.CalculateCardEffect(card, elements);
                                    if (Sum(result) > 3)
                                        res.Append(i).Append(',').Append(j).Append(',').Append(k).Append(',').Append('\t').
                                            Append(l).Append(',').Append(m).Append(',').Append(n).Append(',').Append('\t').
                                            Append(result[0]).Append(',').Append(result[1]).Append(',').Append(result[2]).Append(",\t").Append(Sum(result)).Append('\n');
                                }
                            }
                        }
                    }
                }
            }
            System.IO.File.WriteAllText("E:\\Unity Projects\\Garbage.csv", res.ToString());
        }

        public static int Sum(int[] arr)
        {
            return arr[0] + arr[1] + arr[2];
        }

        public static int Sum(Cards.Card arr)
        {
            return arr.FireEffect + arr.WaterEffect + arr.EarthEffect;
        }

        [MenuItem("Test/Test Balance")]
        public static void TestBalance()
        {
            System.Text.StringBuilder res = new System.Text.StringBuilder();
            int[] elements = new int[3];
            Cards.Card card = ScriptableObject.CreateInstance<Cards.Card>();
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    for (int k = 0; k < 4; k++)
                    {
                        for (int l = 0; l < 4; l++)
                        {
                            for (int m = 0; m < 4; m++)
                            {
                                for (int n = 0; n < 4; n++)
                                {
                                    elements[0] = i;
                                    elements[1] = j;
                                    elements[2] = k;
                                    card.FireEffect = l;
                                    card.WaterEffect = m;
                                    card.EarthEffect = n;
                                    int a = Sum(elements), b = Sum(card);
                                    if (a == 0 || a > 3 || b == 0 || b > 3)
                                        continue;
                                    int[] result = GameUtils.CalculateCardEffect(card, elements);
                                    if (result[0] == 1 && result[1] == 1 && result[2] == 1)
                                        res.Append(i).Append(',').Append(j).Append(',').Append(k).Append(',').Append('\t').
                                            Append(l).Append(',').Append(m).Append(',').Append(n).Append(',').Append('\t').
                                            Append(result[0]).Append(',').Append(result[1]).Append(',').Append(result[2]).Append(",\t").Append(Sum(result)).Append('\n');
                                }
                            }
                        }
                    }
                }
            }
            System.IO.File.WriteAllText("E:\\Unity Projects\\Garbage.csv", res.ToString());
        }
    }
}