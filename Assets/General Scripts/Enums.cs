﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//fire beats earth, earth beats water, water beats fire
public enum Element { Fire, Water, Earth }