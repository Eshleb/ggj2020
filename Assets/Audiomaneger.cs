﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Game
{
    public class Audiomaneger : MonoBehaviour
    {
        public List<int> RoundsToAdvanceMusic;


        int counter = 0;
        public AudioSource[] audios;
        public static Audiomaneger Instance;

        void Awake()
        {
            if (Instance == null)
                Instance = this;
            else
                Destroy(gameObject);
        }
        public void Changemusic(int currentRound)
        {
            if (0 == RoundsToAdvanceMusic.Count)
                return;

            if (currentRound == RoundsToAdvanceMusic[0])
            {
                if (counter < 2)
                {
                    StartCoroutine(_FadeSound());
                }
                RoundsToAdvanceMusic.RemoveAt(0);
            }
        }
        IEnumerator _FadeSound()
        {
            float tDown = 3;
            float tUp = 0;
            while (tDown > 0)
            {
                yield return null;
                tDown -= Time.deltaTime;
                tUp += Time.deltaTime;
                audios[counter].volume = tDown / 3;
                audios[counter+1].volume = tUp / 3;
            }
            counter++;
            yield break;
        }
    
    }
}

