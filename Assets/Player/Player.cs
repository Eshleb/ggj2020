﻿using Cards;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using World;
using Game;

namespace Game
{
    public class Player : MonoBehaviour
    {
        public HandDisplay HandCardsDisplay;

        public Element Element;

        public DrawnCardBehaviour PlayedCard;
        public WorldTile PlayedCardTarget;
        public bool IsReady { get; private set; }
        public static Player CurrentlyPlaying
        {
            get { return currentlyPlaying; }
            set
            {
                if (null != currentlyPlaying)
                    currentlyPlaying.TurnEnd();
                currentlyPlaying = value;
                if (null != value)
                    value.StartShopPhase();
            }
        }
        private static Player currentlyPlaying;

        public Card InitialFireCard, InitialWaterCard, InitialEarthCard;
        
        public Deck Deck;
        public event System.Action OnConfirmAction;
        
        private List<Card> hand;

        void Start()
        {
            GameLoopManager.AddPlayer(this);
            GameLoopManager.OnRoundResolved += NewRound;
            hand = new List<Card>();
            Deck = GetComponent<Deck>();
            switch (Element)
            {
                case Element.Fire:
                Deck.AddToDeck(CardBuilder.Instance.RandomizeHitzone(InitialFireCard));
                Deck.AddToDeck(CardBuilder.Instance.RandomizeHitzone(InitialFireCard));
                break;
                case Element.Water:
                Deck.AddToDeck(CardBuilder.Instance.RandomizeHitzone(InitialWaterCard));
                Deck.AddToDeck(CardBuilder.Instance.RandomizeHitzone(InitialWaterCard));
                break;
                case Element.Earth:
                Deck.AddToDeck(CardBuilder.Instance.RandomizeHitzone(InitialEarthCard));
                Deck.AddToDeck(CardBuilder.Instance.RandomizeHitzone(InitialEarthCard));
                break;
            }
        }

        private void NewRound()
        {
            IsReady = false;
        }

        public void StartShopPhase()
        {
            Shop.Instance.OnCardChosen += StartPlayPhase;
            UIManager.Instance.SetConfirmButtonInteractable(false);
        }

        public void StartPlayPhase(Card card)
        {
            Shop.Instance.OnCardChosen -= StartPlayPhase;
            Deck.AddToGraveyard(card);

            hand.AddRange(Deck.Draw(3));
            HandCardsDisplay.AddCards(hand, this);
        }

        public void TurnEnd()
        {
            Deck.AddToGraveyard(hand);
            hand.Clear();
            HandCardsDisplay.ClearCards();
        }

        public void SaveAction(DrawnCardBehaviour card, WorldTile tile)
        {
            PlayedCard = card;
            PlayedCardTarget = tile;
        }

        public void UndoSavedAction()
        {
            PlayedCard = null;
            PlayedCardTarget = null;
        }

        public void ConfirmAction()
        {
            IsReady = true;
            hand.Remove(PlayedCard.CardObject);
            Deck.AddToGraveyard(PlayedCard.CardObject);
            CardApplicator.Instance.AddCard(PlayedCard.CardObject, PlayedCardTarget);
            OnConfirmAction?.Invoke();
            WorldMap.Instance.RemoveHighlightTiles();

        }
    }
}