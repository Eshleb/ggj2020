﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shapes : MonoBehaviour
{
    private bool[,] hitZone = new bool[2, 2];
    public static Shapes instance;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
            Destroy(this.gameObject);
    }

        public bool[,] ChooseShape()
    {
        bool[,] randomhitZone = hitZone;
        int shape = Random.Range(0, 10);
        switch (shape)
        {
            case 0:
                randomhitZone[1, 0] = true;
                break;
            case 1:
                randomhitZone[1, 0] = true;
                randomhitZone[1, 1] = true;
                randomhitZone[2, 0] = true;
                randomhitZone[2, 1] = true;
                break;
            case 2:
                randomhitZone[0, 0] = true;
                randomhitZone[1, 0] = true;
                randomhitZone[2, 0] = true;
                break;
            case 3:
                randomhitZone[1, 0] = true;
                randomhitZone[1, 1] = true;
                randomhitZone[1, 2] = true;
                break;
            case 4:
                randomhitZone[1, 0] = true;
                randomhitZone[1, 1] = true;
                randomhitZone[0, 1] = true;
                randomhitZone[0, 2] = true;
                break;
            case 5:
                randomhitZone[1, 0] = true;
                randomhitZone[1, 1] = true;
                randomhitZone[2, 1] = true;
                randomhitZone[2, 2] = true;
                break;
            case 6:
                randomhitZone[0, 0] = true;
                randomhitZone[1, 0] = true;
                randomhitZone[2, 0] = true;
                randomhitZone[1, 1] = true;
                break;
            case 7:
                randomhitZone[1, 0] = true;
                randomhitZone[0, 1] = true;
                randomhitZone[1, 1] = true;
                randomhitZone[2, 1] = true;
                break;
            case 8:
                randomhitZone[1, 0] = true;
                randomhitZone[1, 1] = true;
                randomhitZone[1, 2] = true;
                randomhitZone[0, 0] = true;
                break;
            case 9:
                randomhitZone[1, 0] = true;
                randomhitZone[1, 1] = true;
                randomhitZone[1, 2] = true;
                randomhitZone[2, 2] = true;
                break;
        }
        return (randomhitZone);
        

    }
  

}
