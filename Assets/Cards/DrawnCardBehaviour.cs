﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using World;

namespace Cards
{
    public class DrawnCardBehaviour : CardBehaviour
    {
        public Game.Player Player;
        public static Card Held { get; private set; }
        [HideInInspector]
        public Vector3 DeckPosition;
        [HideInInspector]
        public Quaternion DeckRotation;

        protected bool isDragged;

        private bool played;

        override protected void Update()
        {
            base.Update();

            if (isDragged)
            {
                if (Input.GetMouseButtonUp(0))
                    CustomMouseUp();
                else
                {
                    targetPosition = Game.MainCamera.InstanceCam.ScreenToWorldPoint(Input.mousePosition);
                    targetPosition.z = 0;
                }
            }
            transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref movementVelocity, 0.1f);
        }

        protected override void OnMouseDown()
        {
            if (null != Player.PlayedCard && this != Player.PlayedCard) //player played another card
                return;

            Held = CardObject;
            if (played)
            {
                SetPlayed(false);
            }

            transform.rotation = Quaternion.identity;

            var tranparentColor = new Color(1, 1, 1, 0.3f);
            GetComponent<SpriteRenderer>().color = tranparentColor;
            HitzoneImageSR.color = tranparentColor;

            isDragged = true;

            collider.enabled = false;
        }

        protected override void OnMouseEnter()
        {
            if (null != Player.PlayedCard && this != Player.PlayedCard) //player played another card
                return;
            if (Held == null)
            {
                Emphasize();
            }
        }
        protected override void OnMouseExit()
        {
            if (null != Player.PlayedCard && this != Player.PlayedCard) //player played another card
                return;
            if (Held == null)
            {
                Deemphasize();
            }
        }

        private void CustomMouseUp()
        {
            if (Held == CardObject)
                Held = null;

            if (WorldTile.Hovered != null)
            {
                SetPlayed(true);
            }

            transform.rotation = DeckRotation;

            Deemphasize();

            isDragged = false;
            collider.enabled = true;
            isDragged = false;

            GetComponent<SpriteRenderer>().color = Color.white;
            HitzoneImageSR.color = Color.white;
            targetPosition = played ? Player.PlayedCardTarget.transform.position + Vector3.back : DeckPosition;
        }

        private void SetPlayed(bool Played)
        {
            played = Played;
            if (played)
            {
                MainSprite.sortingOrder += 3;
                HitzoneImageSR.sortingOrder += 3;
                Deemphasize();
                Deemphasize();
                Player.SaveAction(this, WorldTile.Hovered);
                Game.UIManager.Instance.SetConfirmButtonInteractable(true);
            }
            else
            {
                MainSprite.sortingOrder -= 3;
                HitzoneImageSR.sortingOrder -= 3;
                Emphasize();
                Emphasize();
                Player.UndoSavedAction();
                Game.UIManager.Instance.SetConfirmButtonInteractable(false);
                WorldMap.Instance.RemoveHighlightTiles();
            }
        }
    }
}