﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Cards
{
    public class Deck : MonoBehaviour
    {
        public List<Card> DeckCards;
        public List<Card> Graveyard;


        void Start()
        {
            DeckCards = new List<Card>();
            Graveyard = new List<Card>();
        }


        public List<Card> Draw(int amount)
        {
            var cards = new List<Card>();

            for (int i = 0; i < amount; i++)
            {
                if (DeckCards.Count == 0)
                    ReshuffleGraveyard();

                if (DeckCards.Count == 0)
                    return cards;

                int index = (int)Random.Range(0, DeckCards.Count);
                Card card = DeckCards[index];
                DeckCards.RemoveAt(index);


                cards.Add(card);
            }

            return cards;
        }

        public void AddToDeck(Card card)
        {
            DeckCards.Add(card);
        }
        public void AddToDeck(IEnumerable<Card> cards)
        {
            DeckCards.AddRange(cards);
        }

        public void AddToGraveyard(Card card)
        {
            Graveyard.Add(card);
        }
        public void AddToGraveyard(IEnumerable<Card> cards)
        {
            Graveyard.AddRange(cards);
        }

        public void ClearDeck()
        {
            if (null != DeckCards)
                DeckCards.Clear();
            if (null != Graveyard)
                Graveyard.Clear();
        }

        protected void ReshuffleGraveyard()
        {
            DeckCards.AddRange(Graveyard);
            Graveyard.Clear();
        }

    }
}
