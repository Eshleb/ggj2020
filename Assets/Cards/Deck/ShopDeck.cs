﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Cards
{
    public class ShopDeck : Deck
    {
        public List<Card> Draw(int amount, Element Element)
        {
            List<Card> haveE = new List<Card>(), dontHaveE = new List<Card>();
            Split(Element, haveE, dontHaveE);
            if (haveE.Count < amount)
                ReshuffleGraveyard();
            haveE.Clear();
            dontHaveE.Clear();
            Split(Element, haveE, dontHaveE);
            DeckCards = haveE;
            List<Card> result = Draw(amount);
            for (int i = 0; i < amount; i++)
                result[i] = CardBuilder.Instance.RandomizeHitzone(result[i]);
            DeckCards = haveE.Concat(dontHaveE).ToList();
            return result;
        }

        private void Split(Element Element, List<Card> haveElement, List<Card> dontHaveElement)
        {
            foreach (Card c in DeckCards)
            {
                if (HasElement(c, Element))
                    haveElement.Add(c);
                else
                    dontHaveElement.Add(c);
            }
        }

        private bool HasElement(Card card, Element element)
        {
            switch (element)
            {
                case Element.Fire: return 0 < card.FireEffect;
                case Element.Water: return 0 < card.WaterEffect;
                case Element.Earth: return 0 < card.EarthEffect;
                default: return false;
            }
        }
    }
}