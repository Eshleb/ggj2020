﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cards
{
    public class ApplicatorCardBehaviour : CardBehaviour
    {
        public event System.Action<ApplicatorCardBehaviour> OnGotToTargetPosition;

        public float AnglesPerSecond;

        public World.WorldTile TargetTile;
        public Vector3 DeckPosition;
        public Vector3 TargetScale;

        private bool gotToTargetPosition;

        protected override void Update()
        {
            base.Update();

            transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref movementVelocity, 0.3f);
            if (!gotToTargetPosition)
            {
                float distance = Vector3.SqrMagnitude(transform.position - targetPosition);
                if (distance < 0.2f)
                {
                    gotToTargetPosition = true;
                    OnGotToTargetPosition?.Invoke(this);
                }
            }

            transform.localScale = Vector3.Lerp(transform.localScale, TargetScale, 0.07f);
        }

        protected override void OnMouseDown()
        {
        }

        protected override void OnMouseEnter()
        {
        }

        protected override void OnMouseExit()
        {
        }

        public override void MoveTo(Vector3 target)
        {
            base.MoveTo(target);
            gotToTargetPosition = false;
        }

        public void Flip(Sprite newSprite, Vector3 newPosition, Vector3 scale, bool showHitzone)
        {
            gotToTargetPosition = false;
            StopAllCoroutines();
            targetPosition = newPosition;
            TargetScale = scale;
            StartCoroutine(FlipAndMoveToPosition(newSprite, showHitzone));
        }

        private IEnumerator FlipAndMoveToPosition(Sprite newSprite, bool showHitzone)
        {
            while (true)
            {
                transform.rotation = Quaternion.AngleAxis(AnglesPerSecond * Time.deltaTime, Vector3.up) * transform.rotation;
                if (90 <= Quaternion.Angle(transform.rotation, Quaternion.identity))
                    break;
                yield return null;
            }
            transform.rotation = Quaternion.AngleAxis(90, Vector3.up);
            MainSprite.sprite = newSprite;
            HitzoneImageSR.enabled = showHitzone;
            while (true)
            {
                transform.rotation = Quaternion.AngleAxis(-AnglesPerSecond * Time.deltaTime, Vector3.up) * transform.rotation;
                if (0 <= Quaternion.Angle(transform.rotation, Quaternion.identity))
                    break;
                yield return null;
            }
            transform.rotation = Quaternion.identity;
        }
    }
}