﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cards;

namespace Game
{
	public class CardApplicator : MonoBehaviour
	{
        #region Singleton
        public static CardApplicator Instance { get; private set; }

        private void Awake()
        {
            if (null == Instance)
                Instance = this;
            else throw new System.Exception("More than one card applicator. ");
        }
        #endregion

        public GameObject CardPrefab;
        public Transform CardPositions;
        public Sprite CardBackSprite;

        public Vector3 HandScale, PlayedScale;
        public float Margin;

        private Queue<ApplicatorCardBehaviour> cardQueue = new Queue<ApplicatorCardBehaviour>();

        public void AddCard(Card card, World.WorldTile Target)
        {
            ApplicatorCardBehaviour newCard = Instantiate(CardPrefab, Target.transform.position, Quaternion.identity).GetComponent<ApplicatorCardBehaviour>();
            newCard.SetCard(card);
            newCard.TargetTile = Target;

            newCard.SetRenderOrder(10 - cardQueue.Count);
            newCard.Flip(CardBackSprite, CardPositions.position + Margin * cardQueue.Count * Vector3.left, HandScale, false);
            cardQueue.Enqueue(newCard);
        }

        public void ApplyCard()
        {
            ApplicatorCardBehaviour card = cardQueue.Dequeue();
            card.OnGotToTargetPosition += SendEffectToWorldMap;
            card.Flip(card.CardObject.CardImage, card.TargetTile.transform.position, PlayedScale, true);
        }

        private void SendEffectToWorldMap(ApplicatorCardBehaviour card)
        {
            World.WorldMap.Instance.Affect(card.CardObject, card.TargetTile);
            GameLoopManager.ApplyNextCard();
            card.Die(0.4f);
        }

        public bool Done { get { return 0 == cardQueue.Count; } }
 	}
}