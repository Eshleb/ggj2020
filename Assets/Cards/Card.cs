﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using World;

namespace Cards
{
    [CreateAssetMenu(fileName = "New Card", menuName = "Card")]
    public class Card : ScriptableObject
    {
        public int Cost;
        public int FireEffect, WaterEffect, EarthEffect;
        public Sprite CardImage;
        public Sprite HitZoneImage;
        public Hitzone hitZone;

        public Card Copy(Card Other)
        {
            Cost = Other.Cost;
            FireEffect = Other.FireEffect;
            WaterEffect = Other.WaterEffect;
            EarthEffect = Other.EarthEffect;
            CardImage = Other.CardImage;
            HitZoneImage = Other.HitZoneImage;
            hitZone = new Hitzone(Other.hitZone);
            name = Other.name;
            return this;
        }
    }
}
