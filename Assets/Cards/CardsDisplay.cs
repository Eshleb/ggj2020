﻿using Cards;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game {
    public abstract class CardsDisplay : MonoBehaviour
    {
        public float CardSlideSpeed;
        public float NewCardDistance;
        public float CardFadeTime;

        public Transform StartTransform;
        public Transform EndTransform;

        protected Vector3 startPosition
        {
            get { return StartTransform.position; }
        }
        protected Vector3 endPosition
        {
            get { return EndTransform.position; }
        }

        protected Quaternion startRotation
        {
            get { return StartTransform.rotation; }
        }
        protected Quaternion endRotation
        {
            get { return EndTransform.rotation; }
        }

        protected List<CardBehaviour> cardBehaviours = new List<CardBehaviour>();

        protected float margin
        {
            get
            {
                return (endPosition.x - startPosition.x) / ((cardBehaviours.Count - 1) > 1 ? cardBehaviours.Count - 1 : 1);
            }
        }

        public abstract void AddCard(Card card, Player Player);

        public void AddCards(IEnumerable<Card> cards, Player Player)
        {
            foreach (var card in cards)
                AddCard(card, Player);
        }

        public void ClearCards()
        {
            while (0 < cardBehaviours.Count)
            {
                var card = cardBehaviours[0];
                cardBehaviours.RemoveAt(0);
                card.Die(CardFadeTime);
            }
        }
    }
}