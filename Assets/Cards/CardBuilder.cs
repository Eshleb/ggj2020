﻿#pragma warning disable 649

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cards
{
    public class CardBuilder : MonoBehaviour
    {
        public GameObject DrawnCardPrefab;
        public GameObject ShopCardPrefab;

        [SerializeField]
        HitZoneScript[] hitzones;
        [SerializeField]
        Card[] cards;

        public static CardBuilder Instance;

        void Awake()
        {
            if (Instance == null)
                Instance = this;
            else
                Destroy(gameObject);
        }

        //public Card GenerateCard()
        //{
        //    Card Newcard = cards[Random.Range(0, cards.Length)];
        //    HitZoneScript hit = hitzones[Random.Range(0, hitzones.Length)];
        //    Newcard.hitZone = hit.hitZone;
        //    Newcard.HitZoneImage = hit.spriteHitzones;
        //    //Newcard.hitZone = Shapes.instance.ChooseShape();
        //    return Newcard;
        //}

        //public CardBehaviour CreateCardGameObject(Card card, Vector3 position, Transform parentTransform)
        //{
        //    var cardPrefab = Instantiate(DrawnCardPrefab, position, Quaternion.identity, parentTransform);
        //    var behaviour = cardPrefab.GetComponent<CardBehaviour>();
        //    behaviour.SetCard(card);

        //    return behaviour;
        //}

        public Card RandomizeHitzone(Card target)
        {
            target = ScriptableObject.CreateInstance<Card>().Copy(target);
            HitZoneScript hit = hitzones[Random.Range(0, hitzones.Length)];
            
            target.hitZone = new Hitzone(hit.hitZone);
            target.HitZoneImage = hit.spriteHitzones;
            return target;
        }

        public DrawnCardBehaviour CreateDrawnCard(Card card, Vector3 position, Transform parentTransform, Game.Player Player)
        {
            GameObject cardPrefab = Instantiate(DrawnCardPrefab, position, Quaternion.identity, parentTransform);
            DrawnCardBehaviour behaviour = cardPrefab.GetComponent<DrawnCardBehaviour>();
            behaviour.SetCard(card);
            behaviour.Player = Player;

            return behaviour;
        }

        public ShopCardBehaviour CreateShopCard(Card card, Vector3 position, Transform parentTransform)
        {
            GameObject cardPrefab = Instantiate(ShopCardPrefab, position, Quaternion.identity, parentTransform);
            ShopCardBehaviour behaviour = cardPrefab.GetComponent<ShopCardBehaviour>();
            behaviour.SetCard(card);

            return behaviour;
        }

    }
}
