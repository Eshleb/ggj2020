﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cards;

namespace Game
{
    public class HandDisplay : CardsDisplay
    {
        public float CardAddSpeed;

        public override void AddCard(Card card, Player Player)
        {
            Vector3 newCardPosition = endPosition + (endPosition - startPosition).normalized * NewCardDistance;

            cardBehaviours.Add(CardBuilder.Instance.CreateDrawnCard(card, newCardPosition, transform, Player));

            for (int i = 0; i < cardBehaviours.Count; i++)
            {
                var pos = cardBehaviours[i].transform.position;
                var rot = Quaternion.Lerp(startRotation, endRotation, (float)i / ((float)cardBehaviours.Count - 1));

                Vector3 deckPosition = new Vector3(startPosition.x + margin * i, pos.y, pos.z);

                float lerpFac = (deckPosition.x - startPosition.x) / (endPosition.x - startPosition.x);
                float startY = Mathf.Tan(startRotation.z) * (deckPosition.x - startPosition.x);
                float endY = Mathf.Tan(-endRotation.z) * (endPosition.x - deckPosition.x);
                float targetY = startY * (1 - lerpFac) + endY * lerpFac;

                deckPosition.y += targetY;

                cardBehaviours[i].MoveTo(pos);
                GameManager.Instance.DelayAction(i * CardAddSpeed, cardBehaviours[i].MoveTo, deckPosition);

                cardBehaviours[i].transform.rotation = rot;
                (cardBehaviours[i] as DrawnCardBehaviour).DeckPosition = deckPosition;
                (cardBehaviours[i] as DrawnCardBehaviour).DeckRotation= rot;
            }
        }
    }
}