﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cards;
using System.Linq;

namespace Game
{
    public class Shop : MonoBehaviour
    {
        public static Shop Instance;

        [System.Serializable]
        public class Inventory
        {
            public int StartRound;
            public List<Card> Cards;

            public Inventory() { }

            public Inventory(Inventory Other)
            {
                StartRound = Other.StartRound;
                Cards = new List<Card>(Other.Cards);
            }
        }

        public List<Inventory> CardInventories;
        private int currentInventoryIndex;
        //private Inventory currentInventory;
        //private Card[] currentShop = new Card[3];

        public ShopDisplay Display;

        public event System.Action<Card> OnCardChosen;

        void Awake()
        {
            if (Instance == null)
                Instance = this;
            else
                Destroy(gameObject);

            Deck = GetComponent<ShopDeck>() ?? gameObject.AddComponent<ShopDeck>();
            GameLoopManager.OnNewTurn += TurnStart;
            //UIManager.Instance.ReadyForNextTurn += TurnStart;

            //Game.GameLoopManager.OnNewRound += SelectCurrentInventory;
        }

        private ShopDeck Deck;
        private List<Card> displayedCards;

        private void TurnStart()
        {
            ResetShop();
            AddCards();
            displayedCards = Deck.Draw(3, Player.CurrentlyPlaying.Element);
            Display.AddCards(displayedCards, Player.CurrentlyPlaying);
        }

        public void TakeCard(Card targetCard)
        {
            Display.ClearCards();
            Deck.AddToGraveyard(displayedCards);

            OnCardChosen?.Invoke(targetCard);
        }

        private void AddCards()
        {
            if (currentInventoryIndex >= CardInventories.Count)
                return;
            if (CardInventories[currentInventoryIndex].StartRound <= GameLoopManager.CurrentRound)
            {
                Deck.AddToDeck(CardInventories[currentInventoryIndex].Cards);
                currentInventoryIndex++;
            }
        }

        void ResetShop()
        {
            if (null != displayedCards)
                displayedCards.Clear();
            Display.ClearCards();
        }


    }
}