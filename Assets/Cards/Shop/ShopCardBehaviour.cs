﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cards
{
    public class ShopCardBehaviour : CardBehaviour
    {
        override protected void Update()
        {
            base.Update();

            transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref movementVelocity, 0.1f);
        }

        protected override void OnMouseDown()
        {
            Game.Shop.Instance.TakeCard(CardObject);
        }

        protected override void OnMouseEnter()
        {
            Emphasize();
        }
        protected override void OnMouseExit()
        {
            Deemphasize();
        }
    }
}