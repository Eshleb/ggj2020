﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class ShopDisplay : CardsDisplay
    {
        public override void AddCard(Cards.Card card, Game.Player Player)
        {
            Vector3 newCardPosition = endPosition + (endPosition - startPosition).normalized * NewCardDistance;

            cardBehaviours.Add(Cards.CardBuilder.Instance.CreateShopCard(card, newCardPosition, transform));

            for (int i = 0; i < cardBehaviours.Count; i++)
            {
                var pos = cardBehaviours[i].transform.position;
                var rot = Quaternion.Lerp(startRotation, endRotation, (float)i / (float)cardBehaviours.Count);

                cardBehaviours[i].MoveTo(new Vector3(startPosition.x + margin * i, pos.y, pos.z));
                cardBehaviours[i].transform.rotation = rot;
            }
        }
    }
}