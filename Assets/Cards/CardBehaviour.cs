﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using World;
using Game;

namespace Cards
{
    public abstract class CardBehaviour : MonoBehaviour
    {
        public Card CardObject;
        public SpriteRenderer MainSprite, HitzoneImageSR;

        public float MovementSpeed;

        protected Vector3 targetPosition;

        protected Vector3 movementVelocity;
        protected new BoxCollider2D collider;

        // Fields for death
        protected bool die;
        protected float fadeStartTime;
        protected float fadeTimer;


        void Start()
        {
            //Debug.Log(Physics.queriesHitTriggers);
            //Physics.queriesHitTriggers = true;
            MainSprite = GetComponent<SpriteRenderer>();
            collider = GetComponent<BoxCollider2D>();
            //targetPosition = transform.position;
        }

        protected virtual void Update()
        {
            if (die)
            {
                fadeTimer -= Time.deltaTime;

                if (fadeTimer < 0)
                    fadeTimer = 0;

                Color fadeColor = new Color(1, 1, 1, fadeTimer / fadeStartTime);
                HitzoneImageSR.color = fadeColor;
                GetComponent<SpriteRenderer>().color = fadeColor;

                if (fadeTimer == 0)
                    Destroy(gameObject);
            }
        }

        public void SetCard(Card card)
        {
            CardObject = card;
            MainSprite.sprite = CardObject.CardImage;
            HitzoneImageSR.sprite = CardObject.HitZoneImage;
        }

        public virtual void MoveTo(Vector3 target)
        {
            targetPosition = target;
        }

        public void Die(float fadeTime)
        {
            fadeStartTime = fadeTime;
            fadeTimer = fadeStartTime;
            die = true;
        }

        public void SetSprite(Sprite Sprite)
        {
            MainSprite.sprite = Sprite;
        }

        public void SetRenderOrder(int order)
        {
            MainSprite.sortingOrder = order;
            HitzoneImageSR.sortingOrder = order;
        }

        protected abstract void OnMouseDown();

        protected abstract void OnMouseEnter();

        protected abstract void OnMouseExit();

        protected void Emphasize()
        {
            transform.localScale += new Vector3(0.3f, 0.3f, 0.3f);
            transform.position -= new Vector3(0, 0, 1);
        }
        protected void Deemphasize()
        {
            transform.localScale -= new Vector3(0.3f, 0.3f, 0.3f);
            transform.position += new Vector3(0, 0, 1);
        }
    }

}
