﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cards
{
    [CreateAssetMenu(fileName = "New HitZone", menuName = "HitZone")]
    public class HitZoneScript : ScriptableObject
    {
        public Hitzone hitZone;
        public Sprite spriteHitzones;
    }
}
