﻿using Cards;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace World
{
    public class WorldMap : MonoBehaviour
    {
        #region Singleton
        public static WorldMap Instance { get; private set; }

        void Awake()
        {
            if (null != Instance)
                throw new System.Exception("More than one worldmap.");
            Instance = this;
        }
        #endregion

        public GameObject TilePrefab;
        public WorldTile[,] Tiles;

        public Transform TopLeftCorner;
        public float Margin;

        public Sprite FireTile;
        public Sprite WaterTile;
        public Sprite EarthTile;
        public Sprite FireWaterTile;
        public Sprite VolcanoTile;
        public Sprite IslandTile;
        public Sprite HarmonyTile;
        public Sprite EmptyTile;

        public TMPro.TextMeshProUGUI waterText, fireText, earthText;

        private List<WorldTile> highlitedTiles;

        public int RoundsUntilImpact, randomEventElement, randomEventCorner;
        public int fireSum, waterSum, earthSum;

        private void Start()
        {
            highlitedTiles = new List<WorldTile>();

            int gridHeight = 6,
                gridWidth = 6;

            Tiles = new WorldTile[gridHeight, gridWidth];

            float TileSize = 125f / 125f;

            Vector3 startPos = TopLeftCorner.position;
            startPos.x += TileSize / 2.0f;
            startPos.y -= TileSize / 2.0f;

            TileSize += Margin;

            for (int i = 0; i < gridWidth; i++)
                for (int j = 0; j < gridHeight; j++)
                {
                    var newPosition = startPos + new Vector3(TileSize * i, -TileSize * j, 0);
                    Tiles[i, j] = Instantiate(TilePrefab, newPosition, Quaternion.identity, transform).GetComponent<WorldTile>();
                    Tiles[i, j].AssignInitialProminences(0, 0, 0);
                    Tiles[i, j].Position.Set(i, j);
                }

            waterText.text = "0";
            fireText.text = "0";
            earthText.text = "0";
        }

        public void HighlightTiles(Card heldCard)
        {
            RemoveHighlightTiles();
            highlitedTiles = Utils.GameUtils.GetAffectedTiles(Tiles, heldCard.hitZone, WorldTile.Hovered.Position);

            foreach (var tile in highlitedTiles)
                tile.Highlight();
        }

        public void RemoveHighlightTiles()
        {
            foreach (var tile in highlitedTiles)
                tile.RemoveHighlight();

            highlitedTiles.Clear();
        }
        
        public int[] GetElementalBalance()
        {
            ChangeElementValueNumber();
            int[] elementalSum = new int[3] { fireSum, waterSum, earthSum};
            return elementalSum;
        }

        public void ChangeElementValueNumber()
        {
            fireSum = 0; waterSum = 0; earthSum = 0;
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    fireSum += Tiles[i, j].ElementProminence[0];
                    waterSum += Tiles[i, j].ElementProminence[1];
                    earthSum += Tiles[i, j].ElementProminence[2];
                }
            }
            waterText.text = waterSum.ToString();
            fireText.text = fireSum.ToString();
            earthText.text = earthSum.ToString();
        }

        public Sprite GetSprite(int fire, int water, int earth)
        {
            if (fire == 0)
            {
                if (water == 0)
                    return earth == 0 ? EmptyTile : EarthTile;
                else
                    return earth == 0 ? WaterTile : IslandTile;
            }
            else
            {
                if (water == 0)
                    return earth == 0 ? FireTile : VolcanoTile;
                else
                    return earth == 0 ? FireWaterTile : HarmonyTile;
            }
        }
        public void SetSprite(WorldTile tile)
        {
            int fire = tile.ElementProminence[(int)Element.Fire];
            int water = tile.ElementProminence[(int)Element.Water];
            int earth = tile.ElementProminence[(int)Element.Earth];

            tile.SetSprite(GetSprite(fire, water, earth));
        }

        public void Affect(Card card, WorldTile anchorTile)
        {
            List<WorldTile> affectedTiles = Utils.GameUtils.GetAffectedTiles(Tiles, card.hitZone, anchorTile.Position);

            foreach (var tile in affectedTiles)
                tile.Affect(card);
        }

        public void AnnounceRandomEvent()
        {
            randomEventElement = Random.Range(0, 2);
            randomEventCorner = Random.Range(0, 3);
            string corner;
            string effect;
            string announcement;
            switch (randomEventElement)
            {
                case 0:
                    effect = "heatwave";
                    break;
                case 1:
                    effect = "hurricane";
                    break;  
                case 2:
                    effect = "sandstorm";
                    break;
                default:
                    effect = "error";
                    break;
            }
            switch (randomEventCorner)
            {
                case 0:
                    corner = "upper left";
                    break;
                case 1:
                    corner = "lower left";
                    break;
                case 2:
                    corner = "upper right";
                    break;
                case 3:
                    corner = "lower right";
                    break;
                default:
                    corner = "error";
                    break;
            }
            RoundsUntilImpact = Random.Range(3, 6);
            announcement = "A " + effect + " is about to hit the " + corner + " corner in " + RoundsUntilImpact + " rounds!";
            Game.Announcer.Instance.Announce(2.5f, 1f, effect.ToUpper() + " INCOMING!", announcement);
        }

        public void PlayRandomEvent()
        {
            Color color;
            switch (randomEventElement)
            {
                case 0:
                    color = new Color(1, 0.4f, 0);
                    break;
                case 1:
                    color = new Color(0.17f, 0.27f, 0.87f);
                    break;
                case 2:
                    color = new Color(0.57f, 0.43f, 0f);
                    break;
                default: color = Color.white;
                    break;
            }
            Game.MainCamera.Instance.FlashColor(color, 0.6f, 0.5f);
            switch (randomEventCorner)
            {
                //Upper Left Corner
                case 0:
                    for (int i = 0; i < 3; i++)
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            Tiles[i, j].AddStackOfElement((Element)randomEventElement);
                        }
                    }
                    break;
                //Lower Left Corner
                case 1:
                    for (int i = 0; i < 3; i++)
                    {
                        for (int j = 3; j < 6; j++)
                        {
                            Tiles[i, j].AddStackOfElement((Element)randomEventElement);
                        }
                    }
                    break;
                //Upper Right Corner
                case 2:
                    for (int i = 3; i < 6; i++)
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            Tiles[i, j].AddStackOfElement((Element)randomEventElement);
                        }
                    }
                    break;
                //Lower Right Corner
                case 3:
                    for (int i = 3; i < 6; i++)
                    {
                        for (int j = 3; j < 6; j++)
                        {
                            Tiles[i, j].AddStackOfElement((Element)randomEventElement);
                        }
                    }
                    break;
                default:
                    break;
            }
        }


        //public void RandomizeTiles(List<WorldTile> tiles)
        //{
        //    int playerFullHealth = tiles.Count;

        //    Dictionary<Element, int> healthBars = new Dictionary<Element, int>()
        //    {
        //        {Element.Fire, playerFullHealth},
        //        {Element.Water, playerFullHealth},
        //        {Element.Earth, playerFullHealth}
        //    };

        //    for (int i = 0; i < tiles.Count; i++)
        //    {
        //        int points = 3;
        //        var elementOrder = new List<Element>()
        //        {
        //            Element.Fire, Element.Water, Element.Earth
        //        };

        //        while (elementOrder.Count > 0 && points > 0)
        //        {
        //            var currentElement = elementOrder[Random.Range(0, elementOrder.Count)];
        //            elementOrder.Remove(currentElement);

        //            int pointsDif = Random.Range(0, )
        //        }

        //    }
        //}
    }
}