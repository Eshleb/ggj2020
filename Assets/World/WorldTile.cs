﻿using Cards;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace World
{
    public class WorldTile : MonoBehaviour
    {
        public static WorldTile Hovered { get; private set; }
        public int[] ElementProminence;
        public Vector2Int Position;

        public void AssignInitialProminences(int fireProm, int waterProm, int earthProm)
        {
            ElementProminence = new int[3];
            ElementProminence[(int)Element.Fire] = fireProm;
            ElementProminence[(int)Element.Water] = waterProm;
            ElementProminence[(int)Element.Earth] = earthProm;
        }

        public void Affect(Card Card)
        {
            ElementProminence = Utils.GameUtils.CalculateCardEffect(Card, ElementProminence);

            WorldMap.Instance.SetSprite(this);
        }

        public void AddStackOfElement(Element element)
        {
           Utils.GameUtils.ApplyEffect(element, ElementProminence);

            WorldMap.Instance.SetSprite(this);
        }

        public void SetSprite(Sprite sprite)
        {
            GetComponent<SpriteRenderer>().sprite = sprite;
        }

        public void Highlight()
        {
            GetComponent<SpriteRenderer>().color = new Color(1, 0, 0);
        }
        public void RemoveHighlight()
        {
            GetComponent<SpriteRenderer>().color = Color.white;
        }

        private void OnMouseEnter()
        {
            Hovered = this;
            if (DrawnCardBehaviour.Held != null)
                WorldMap.Instance.HighlightTiles(DrawnCardBehaviour.Held);
            else
            {
                HoverTile.Instance.ShowMeter(ElementProminence);
            }
        }
        private void OnMouseExit()
        {
            if (this == Hovered)
            {
                Hovered = null;
                HoverTile.Instance.HideMeter();
                
                if (null== Game.Player.CurrentlyPlaying || null == Game.Player.CurrentlyPlaying.PlayedCard)
                    WorldMap.Instance.RemoveHighlightTiles();
            }
        }
    }
}