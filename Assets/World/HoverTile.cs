﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace World
{

    public class HoverTile : MonoBehaviour
    {
        public float TimeBeforeAppears;
        public SpriteRenderer[] elementalGem;
        public static HoverTile Instance;
        public Sprite [] elements;
        
        int elementalPower = 0;
        int jemCounter = 0;
        Coroutine hoverPopUp;
     
        void Awake()
        {
            if (Instance == null)
                Instance = this;
            else
                Destroy(gameObject);
        }

        // Update is called once per frame
        public void ShowMeter(int[] ElementProminence)
        {
            hoverPopUp = StartCoroutine(Openhover(ElementProminence));
            this.gameObject.transform.position = new Vector3(100, 100, 100);
        }
       public void HideMeter()
        {
            jemCounter = 0;
            if(hoverPopUp!=null)
            StopCoroutine(hoverPopUp);
            for (int i = 0; i < 3; i++)
            {
                elementalGem[i].sprite= elements[3];
            }
            this.gameObject.transform.position = new Vector3(100, 100, 100);
        }

        public IEnumerator Openhover(int[] ElementProminence)
        {
            yield return new WaitForSeconds(TimeBeforeAppears);
            this.gameObject.SetActive(true);
            for (int i = 0; i < 3; i++)
            {
                elementalPower = ElementProminence[i];
                for (int j = 0; j < elementalPower; j++)
                {
                    elementalGem[jemCounter].sprite = elements[i];
                    jemCounter++;
                }
            }
            if (WorldTile.Hovered != null)
            {
                this.gameObject.transform.position =( WorldTile.Hovered.gameObject.transform.position + new Vector3(3, -0.5f, 0));
            }
        }
    }
}
